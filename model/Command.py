import subprocess


class Command:

    def __init__(self, name, command):
        self.name = name
        self.command = command

    def execute(self):
        return subprocess.Popen(self.command.split(), stdout=subprocess.PIPE).communicate()
