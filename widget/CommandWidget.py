from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QWidget, QLabel, QGridLayout, QPushButton


class CommandWidget(QWidget):

    def __init__(self, command):
        super().__init__()
        self.command = command
        self.grid = QGridLayout()
        self.initUI()

    def initUI(self):
        self.setLayout(self.grid)
        self.grid.setSpacing(20)

        self.nameFont = QFont("Avenir", 14, QFont.Bold)
        self.cmdFont = QFont("Courier", 13)

        self.nameLabel = QLabel(self.command.name)
        self.nameLabel.setFont(self.nameFont)

        self.cmdLabel = QLabel(self.command.command)
        self.cmdLabel.setFont(self.cmdFont)
        self.cmdLabel.setFixedWidth(600)

        self.execButton = QPushButton('exec', self)
        self.execButton.setFixedSize(70, 30)
        self.execButton.clicked.connect(self.execCmd)

        self.grid.addWidget(self.nameLabel, 0, 0)
        self.grid.addWidget(self.cmdLabel, 1, 0)
        self.grid.addWidget(self.execButton, 0, 1)
        self.show()

    def execCmd(self):
        print(self.command.execute()[0].decode("utf-8"))
