from PyQt5.QtCore import Qt, QRect
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel, QScrollArea


class MainWindow(QWidget):

    def __init__(self):
        super().__init__()
        self.grid = QGridLayout()
        self.rowCounter = 0
        self.initUI()

    def initUI(self):
        self.resize(650, 400)
        self.move(50, 50)
        self.setWindowTitle('Quick Control')

        self.grid.setAlignment(Qt.AlignTop)
        self.setLayout(self.grid)

        self.show()

    def addCommandWidget(self, cmdWidget):
        self.grid.addWidget(cmdWidget, self.rowCounter, 0)
        self.rowCounter += 1
