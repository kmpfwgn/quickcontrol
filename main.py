import sys

from PyQt5.QtWidgets import QApplication

from MainWindow import MainWindow
from model.Command import Command

if __name__ == '__main__':
    app = QApplication(sys.argv)

    mw = MainWindow()

    cmd = Command("cmd1", "apt")

    sys.exit(app.exec_())
